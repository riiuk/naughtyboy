﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDraw : MonoBehaviour {

	//objetivo del dibujado de la línea

	public GameObject target;

	//referencia al componente LineRenderer
	private LineRenderer lineRenderer;



	// Use this for initialization
	void Start () {

		//recuperamos la referencia al componente LineRenderer
		lineRenderer = GetComponent<LineRenderer> ();

	}
	
	// Update is called once per frame
	void Update () {


		//la posición inicial será el centro del objeto
		lineRenderer.SetPosition (0, transform.position);

		//la posicion de destino será donde se encuentre el objeto target
		lineRenderer.SetPosition (1, target.transform.position);

	}
}
