﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionObjectActivation : Reaction {
	public GameObject targetObject;

	public bool active;
	//bool para indicar si activaremos o desactivaremos el objeto

	protected override IEnumerator React(){
		
		yield return new WaitForSeconds (delay);
		//activa o desactiva el objeto objetivo, segun el parametro active
		targetObject.SetActive (active);

	}
}
