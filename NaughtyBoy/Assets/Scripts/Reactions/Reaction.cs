﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Reaction : MonoBehaviour {
	//descripcion de la reaccion, para uso en el editor
	public string description; 
	//retardo en realizar la reacción
	public float delay;

	//metodo al que llamaremos para hacer las reacciones, será extendido a todas las clases que lo hereden lo hacemos de tipo virtual para poder sobreescribirlo
	public virtual void ExecuteReaction(){
		StartCoroutine (React ());
	}


	//corutina que será ejecutada como reccion
	protected virtual IEnumerator React (){
		
		//retardo de la reacción
		yield return new WaitForSeconds (delay);

	}

}
