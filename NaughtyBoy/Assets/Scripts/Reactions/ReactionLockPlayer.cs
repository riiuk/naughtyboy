﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionLockPlayer : Reaction {

	// Referencia al PlayerMovement
	private PlayerMovement playerMovement;


	// Use this for initialization
	void Start () {
		playerMovement = GameObject.Find ("Player").GetComponent<PlayerMovement> ();	
	}

	/// <summary>
	/// Método que ejecuia la reacción
	/// </summary>
	protected override IEnumerator React() {
		// desactivamos el control por parte del jugador
		playerMovement.handleInput = false;

		yield return new WaitForSeconds (delay);

		playerMovement.handleInput = true;
	}

	void OnDisable() {
		playerMovement.handleInput = true;	
	}

}
