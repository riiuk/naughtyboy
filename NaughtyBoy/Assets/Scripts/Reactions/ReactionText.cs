﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionText : Reaction {

	public string text;

	public Color textColor;


	/// <summary>
	/// metodo que ejecuta la reaccion con override para que pise la corrutina heredada
	/// </summary>
protected override IEnumerator React(){
		
		yield return new WaitForSeconds (delay);

		TextManager.TM.DisplayMesssage (TranslateManager.In.GetString(text), textColor);
	}
}
