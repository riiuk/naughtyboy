﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionAnimation : Reaction {

	//objeto que será animado
	public GameObject target;

	//nombre del trigger del animator a disparar
	public string triggerName;

	protected override IEnumerator React(){

		//Debug.Log ("Animation Reaction -" + description);

		yield return new WaitForSeconds (delay);

		target.GetComponent<Animator> ().SetTrigger (triggerName);

	}
}
