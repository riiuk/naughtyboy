﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ReactionTakeItem : Reaction {

	//ewferencia del item principal, para recuperar eol nombre dle item
	private GameObject itemReference;
	private string itemName;

	void Awake(){
		//recuperamos la referencia del objeto principal del item
		itemReference = transform.parent.transform.parent.gameObject;

		//con la referencia recuperamos el nombre del item
		itemName = itemReference.GetComponent<InteractableItem>().itemName;
	}
	/// <summary>
	/// Hace la recogida del objeto y añade en el inventario el objeto
	/// </summary>
	private void PickUpItem(){
		//recorremos el array de objetos disponibles en allItems
		foreach(Item item in DataManager.DM.data.allItems){
			//verificamos si el nombre del objeto es igual al que buscamos
			if(item.name == itemName){
				// Intentamos añadir el objeto en el invetario, si se puede, hacemos los pasos de recogida del objeto
				if (InventoryManager.IM.AddItemInventory(itemName)) {
					//marcamos el objeto en allItems como recogido
					item.picked = true;
					//deshabilitamos el objeto padre para que desaparezca de la escena
					itemReference.SetActive (false);
					//salinos del método una vez lo hemos encontrado
				} else {
					// En caso de no poder recoger el objeto, llamamos al TextManager para que muestre un mensaje de aviso
					TextManager.TM.DisplayMesssage (TranslateManager.In.GetString("inventory_full"), Color.white);
				}
			return;
			}
		}
		//si llegamos hasta aqui significa que el objeto buscado no existe, lanzamos un ewarning
		Debug.LogWarning ("el nombre del objeto no existe:" + itemName);

	}
	/// <summary>
	/// Método que ejecuta la reaccion, con override para que pise la corrutina heredada
	/// </summary>
	protected override IEnumerator React(){

		yield return new WaitForSeconds (delay);

		//llamamos al método que realiza las acciones de recogida
		PickUpItem ();
	}
}
