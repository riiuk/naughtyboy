﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionDropItem : Reaction {

	//nombre del item a retirar del inventario
	public string itemName;
	/// <summary>
	/// metodo que ejecuta la reaccion
	/// </summary>
	protected override IEnumerator React(){
		yield return new WaitForSeconds (delay);

		//eliminamos el objeto del inventario
		InventoryManager.IM.RemoveItemInventory (itemName);

	}

}

