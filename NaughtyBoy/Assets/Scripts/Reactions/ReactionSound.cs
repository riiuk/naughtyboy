﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionSound : Reaction {

	//clip de audio a reproducir
	public AudioClip audioClip;
	//referencia al componente audioSource que reproducirá el clip
	private AudioSource audioSource;


	// Use this for initialization
	void Start () {
		//recuperamos la referencia al audioSource
		audioSource = GetComponent<AudioSource> ();

	}
	/// <summary>
	/// Método que ejecuta la reacción, con override para que pise la corrutina heredada
	/// </summary>
	protected override IEnumerator React(){
		//realixamos la espera, el delay viene de la clase padre 
		yield return new WaitForSeconds (delay);

		//le asignamos el clip a reproducir
		audioSource.clip = audioClip;
		//reproducimos el sonido

		audioSource.Play ();
	}

}
