﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionCondition : Reaction {
	//nombre de la condicion a modificar
	public string conditionName;

	//estado al que queremos cambiar la condicion
	public bool conditionStatus;

	/// <summary>
	/// Método que ejecuta la reacción
	/// </summary>
	protected override IEnumerator React(){
		
		yield return new WaitForSeconds (delay);

		foreach (Condition condition in DataManager.DM.data.allCondition) {
			if (condition.name == conditionName) {
				condition.done = conditionStatus;
				//si lo hemos encontrado, salimos del bucle
				break;
			}
		}

	}

}

