﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionScene : Reaction {

	//nombre de la escena a cambiar
	public string nextScene;

	//nombre del punmto de entrada a la escena a la que cambiamos
	public string nextStartPoint;
	/// <summary>
	/// Método que ejecuta la reacción, con override para que pise la corrutina heredada
	/// </summary>
	protected override IEnumerator React(){
		//tiempo de espera
		yield return new WaitForSeconds (delay);

		//llamamos al metodo de sceneController que llevara a cabo la tarea del cambio de escena 
		SceneController.SC.FadeAndLoadScene (nextScene, nextStartPoint);

		}
}
