﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;


public class SceneController : MonoBehaviour {

	//canvas group con el que haremos el escurecido de la pantalla
	public CanvasGroup fadeCanvasGroup;

	//duracion del fade
	public float fadeDuration= 1f;

	//valor por defecto que utilizarremos si no está definida la escena actual en el DataManager
	public string startingSceneName = "blockScene";

	//valor por defecto de la posición de la entrada a la escena, por si no está definida en el DataManger
	public string initialStartingPosition = "ExteriorDoor";

	//bool para indicar cuando se está relizando el fade
	private bool isFading;

	public static SceneController SC;

	void Awake(){
		if (SC == null) {
			SC = GetComponent<SceneController> ();

		}
	}

	private IEnumerator Start(){
		//ponemos la pantalla en negro
		fadeCanvasGroup.alpha = 1f;
		//si existe un actualScene definido en el DataManager lo usamos, en caso contrario usamos el que hemos definido por defecto
		startingSceneName = DataManager.DM.data.actualScene != "" ? DataManager.DM.data.actualScene : startingSceneName;
	
		initialStartingPosition = DataManager.DM.data.startPosition != "" ? DataManager.DM.data.startPosition : initialStartingPosition;

		//cargamos la escena y la ponemos activa
		yield return StartCoroutine (LoadSceneAndSetActive (startingSceneName));
		//hacemos un fade in
		StartCoroutine (Fade (0f));


	}

	/// <summary>
	/// Llamada publica para el cambio de escenas
	/// </summary>
	/// <param name="sceneName">Scene name.</param>
	/// <param name="startPosition">Start position.</param>
	public void FadeAndLoadScene(string sceneName, string startPosition){
		//actualizamos el valor de la escena actual
		DataManager.DM.data.actualScene = sceneName;

		DataManager.DM.data.startPosition = startPosition;
		//guardamos al realizar un cambi ode escena
		DataManager.DM.Save ();
		//si no esta haciendo un fade
		if (!isFading) {
			//llamamos la corrutina para fade y cambio de escena
			StartCoroutine (FadeAndSwitchScenes (sceneName));
		}

	}
	/// <summary>
	/// cambia de escena
	/// </summary>
	/// <returns>The and switch scenes.</returns>
	/// <param name="sceneName">Scene name.</param>
	private IEnumerator FadeAndSwitchScenes(string sceneName){
		//hacemos fade out
		yield return StartCoroutine (Fade(1f));

		//en cuanto termina el Fade, descargamos la escena activa
		yield return SceneManager.UnloadSceneAsync (SceneManager.GetActiveScene ().buildIndex);

		//una vez descargada la escena, llamamos a la corrutina que cargará la nueva escena
		yield return StartCoroutine (LoadSceneAndSetActive (sceneName));

		//una vez terminada la carga, haremos un fade in
		yield return StartCoroutine (Fade (0f));

	}
	/// <summary>
	/// Carga y activa la escena
	/// </summary>
	/// <returns>The scene and set active.</returns>
	/// <param name="sceneName">Scene name.</param>
	private IEnumerator LoadSceneAndSetActive(string sceneName){
		//cargamos la escena de forma Aditiva "Aditiva", sin destruir la escena actual

		yield return SceneManager.LoadSceneAsync (sceneName, LoadSceneMode.Additive);

		//para recuperar la última escena añadida, tomamos el número actual de escenas menos 1, esto dará el indice de la última escena añadida
		Scene newlyLoadedScene = SceneManager.GetSceneAt (SceneManager.sceneCount - 1);

		SceneManager.SetActiveScene  (newlyLoadedScene);
			
	}
	/// <summary>
	/// fundido en negro
	/// </summary>
	/// <param name="finalAlpha">Final alpha.</param>
	private IEnumerator Fade (float finalAlpha){
		//INDICAMOS QUE SE ESTÁ REALIZANDO UN FADE
		isFading = true;
		//bloqueamos todo raycast para evitar que se interactue durante el fade 
		fadeCanvasGroup.blocksRaycasts = true;

		//calculamos la velocidad del fade
		float fadeSpeed = Mathf.Abs (fadeCanvasGroup.alpha - finalAlpha) / fadeDuration;

		//mientras el alpha no sea Similar al alpha objetivo, ejecutamos acciones
		while (!Mathf.Approximately (fadeCanvasGroup.alpha, finalAlpha)) {
			//aumentamos el valor del alpha mediante movetowards
			fadeCanvasGroup.alpha = Mathf.MoveTowards (fadeCanvasGroup.alpha, finalAlpha, fadeSpeed * Time.deltaTime);

			//hacemos que la corrutina se ejecute en cada frame
			yield return null;
		}
		//si llega hasta este punto, es que el fade ha terminado
		isFading = false; 
		//volvemos a pernitir nuevamente los raycast
		fadeCanvasGroup.blocksRaycasts = false;

	}
}