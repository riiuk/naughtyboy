﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//para serializar en binario
using System.Runtime.Serialization.Formatters.Binary;

//para gestión de ficheros
using System.IO;


public class DataManager : MonoBehaviour {

	//objeto que contendrá toda la información de la partida
	public Data data;

	//nombre que tendrá el fichero al gardar
	public string fileName = "data.dat";	

	public static DataManager DM;

	void Awake(){
		//en  caso de que  no exista La instancia
		if (DM == null) {
			//creamos la instancia principal del componente de este objeto
			DM = GetComponent<DataManager> ();

		//recuperamos la informacion almacenada en disco al inicio
		Load ();

		}


	}

	public void Save(){
		//objeto utilizado para selializar/deserializar
		BinaryFormatter bf = new BinaryFormatter ();
		//creamos/sobreescribimos el fichero con los datos
		FileStream file = File.Create (Application.persistentDataPath + "/" + fileName);
		//serializamos el contenido de nuestro objeto de datos
		bf.Serialize (file, data);
		//Cerramos el fichero
		file.Close();

	}

	public void Load(){
		//Debug.Log con la ruta de guardado persistente(para poder localizarlo)
		Debug.Log (Application.persistentDataPath);
		//antes de abrir el fichero verificamos si existe
		if (File.Exists (Application.persistentDataPath + "/" + fileName)) {
			//objeto para serializar/deserializar
			BinaryFormatter bf = new BinaryFormatter ();
			//abrimos el fichero
			FileStream file = File.Open (Application.persistentDataPath + "/" + fileName, FileMode.Open);
			//deserializamos el fichero
			data = (Data)bf.Deserialize (file);
			//cerramos el fichero
			file.Close ();

		}

	}
	/// <summary>
	/// Devuelve el estado en el que se encuentra una condicion global
	/// </summary>
	/// <returns><c>true</c>, if condition was checked, <c>false</c> otherwise.</returns>
	/// <param name="conditionName">Condition name.</param>
	public bool CheckCondition(string conditionName) {
		//buscamos la condicion en la lista
		foreach (var cond in data.allCondition) {
			//si la encuentro, devuelvo el valor de su estado
			if (cond.name == conditionName) {
				return cond.done;
			}
		}

		//si termina el foreach y llega hasta aqui, signiica que no ha encontrado nada
		//mandamos mensaje de debug y devolvemos un false
		Debug.Log (conditionName + "-no existe");
		return false;

	}


	public void SetCondition (string conditionName, bool done){
		//busco la condicion en la lista
		foreach (var cond in data.allCondition) {
			//si la encuentro, le asigno el valor del estado
			if (cond.name == conditionName) {
				cond.done = done;
			}
		}
	}
}
