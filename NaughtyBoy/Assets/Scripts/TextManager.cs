﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//para poder hacer uso de los elementos UI
using UnityEngine.UI;

public class TextManager : MonoBehaviour {

	public Text text; 										//referencia al text del canvas

	public float displayTimePerCharacter = 0.1f; 			//tiempo minimo de mostrado de cada caracter

	public float additionalDisplayTime = 0.5f; 				//tiempo adicional extra mostrado de cada frase


	private float clearTime; 								//tiempo en el que se borrará el texto

	public static TextManager TM;							//referencia estática

	// Use this for initialization
	void Awake () {

		if (TM == null) {
			TM = GetComponent<TextManager> ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		//si el tiempo actual es mayhor que el fijaso para el borrado
		//Realizamos el borrado del texto en la caja de texto
		if (Time.time >= clearTime) {

			text.text = "";

		}
	}
	/// <summary>
	/// Muestra el texto por pantalla, indicando texto a mostrar y color
	/// </summary>
	/// <param name="message">Message.</param>
	/// <param name="textColor">Text color.</param>
	public void DisplayMesssage(string message, Color textColor){

		//calculando la duracion en funcion del numero de caracteres dle mensaje (mas un extra adicional)

		float displayDuration = message.Length * displayTimePerCharacter + additionalDisplayTime;
		//calculamos el timepo exacto para el borrado
		clearTime = Time.time + displayDuration;

		//asignamos el ttexto del mensaje
		text.text = message;
		//cambiamos el color del texto
		text.color = textColor;

	}

}
