﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Condition  {
	
	//nombre de la condicion, que debe ser único
	public string name;

	//descripción de apoyo, solo visible en desarrollo
	public string description;

	//indica si la condición ha sido cumplida o no
	public bool done; 
}
