﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//heredamos de la clase Interactable y añadiremos cualidades extra
public class InteractableItem : Interactable {

	//nombre dle item , con el que buscaremos en la lista de allItems
	public string itemName;


	// Use this for initialization
	void Start () {
		//verificamos si el objeto ya ha sido recogido, 
		if (isPicked()){
			gameObject.SetActive (false);
		}
	}
	/// <summary>
	/// verificamos si el objeto ya ha sido recogido
	/// </summary>
	/// <returns><c>true</c>, if picked was ised, <c>false</c> otherwise.</returns>
	bool isPicked(){
		//recorremos la lista de items defiidos, buscando nuestro item
		foreach (Item item  in DataManager.DM.data.allItems) {
			if (item.name == itemName) {
				//si lo encontramos, terminamos la funcion devolviendo el estado del objeto encontrado
				return item.picked;
			}
		}

		//si llegamos a este punto significa que el objeto no ha sido encontrado,
		//asi que avisamos de que el nombre pueder ser erromeo
		Debug.LogWarning ("El nombre del item no existe en la lista: " + itemName);
		return false;

	}
}
