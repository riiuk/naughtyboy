﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//para poder hacer uso
using System.Xml;

public class TranslateManager : MonoBehaviour {

	//idioma por defecto 
	public string defaultLanguaje = "English";

	//listado de items tipo array,pero que permite indices alfanumericos
	//aqui guardamos los textos del juego
	public Hashtable strings; 

	public static TranslateManager In;



	// Use this for initialization
	void Awake () {
		if (In == null) {
			In = GetComponent<TranslateManager> ();
		}
	}

		void Start(){
		//recuperamos ellenguaje del sistema operativo en un string
		string language = Application.systemLanguage.ToString();
		//recuperamos el fichero como asset de tipo texto
		TextAsset textAsset = (TextAsset)Resources.Load ("Lang", typeof (TextAsset));
		//creamos una variable de tipo xmldocument, par ahacer la gestion del xml
		XmlDocument xml = new XmlDocument ();
		//volcamos el contenido recuperado del fichero del texto, a nuestro objeto xml
		xml.LoadXml (textAsset.text);

		//verificamos si no existe el idioma
		if (xml.DocumentElement [defaultLanguaje] == null) {

			//si no existe el idioma en el xml, usaremos el definido como default
			language = defaultLanguaje;

		}

		SetLanguage (xml, language);

	}

	public void SetLanguage(XmlDocument xml, string language){
		//inicializamos el hashtable
		strings = new Hashtable ();
		//recuperamos el idioma selewccionado
		XmlElement element = xml.DocumentElement [language];

		if (element != null) {
			//mediante este metodo recuperamos un tipo enumerador, que nos permite recorrer el xml como si fuera un foreach
			IEnumerator elemEnum = element.GetEnumerator ();

			//nientras quedan elementos, recuperamos elementos
			while (elemEnum.MoveNext ()) {

				//recuperamos el elemento que contiene el literal actual
				var xmlItem = (XmlElement)elemEnum.Current;

				//añadimos el literal actual
				strings.Add (xmlItem.GetAttribute ("name"), xmlItem.InnerText);

			}
		} else {
			//en caso de no recuperar nada, significa que el idioma no exist e
			Debug.LogWarning ("el idioma especificado no existe: " + language);
		}
	}
	/// <summary>
	/// Recupera un literal de texto de la hash table mediante un indice
	/// </summary>
	/// <returns>The string.</returns>
	/// <param name="name">Name.</param>
	public string GetString(string name){
		//verificamos si no existe el indice solicitado
		if (!strings.ContainsKey (name)) {
			//
			Debug.LogWarning ("El índice no existe: " + name);
			//si no existe, devolvemos una cadena vacía
			return "";
		}

		//si llegamos hasta aqui, es que existe,, asi que devolvemos el valor de ese indice
		return (string)strings [name];

	}
}
		