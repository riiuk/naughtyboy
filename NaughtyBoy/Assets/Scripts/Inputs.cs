﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs : MonoBehaviour {

	public GameObject canvasInventory;
	public bool inventaryOn;


	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Inventory")) {
			InventoryActiveOn ();
		} else {
			InventoryActiveOff ();
		}

		
	}

	void InventoryActiveOn(){

		canvasInventory.SetActive (true);
		inventaryOn = true;
	}

	void InventoryActiveOff() {

		canvasInventory.SetActive (false);
		inventaryOn = false;
	}
}
