﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Item  {

	//nombre del item
	public string name;
	//descripcion del objeto
	public string description;
	// nombre de la imagen para recuperarla de resources
	public string imageName;
	//indica si el objeto ha sido recogido o no,esto será utilizado solo en el conjunto allItems
	public bool picked = false;


	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
