﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {
	[Header("Posición de interacción")]

	//posicion en la que debera ser posicionado el personaje al interactuar con el objeto
	public Transform interactionLocation;

	[Header ("Condiciones")]
	//condiciones a cumplir para que se produzxca las reacciones positivas
	public string[] conditions;

	/*[Header ("Reacciones positivas")]
	//Reaciiones que serán ejecutadas en caso de que se ucumplen las condiciones
	public Reaction [] positiveReactions;


	[Header ("Reacciones por defecto")]
	//Reacciones que serán ejecutadas en caso de que no se cumplan las condiciones
	public Reaction[] defaultReactions;

*/
	/// <summary>
	/// Gestionará las condiciones y las reacciones
	/// </summary>
	public void Interact(){

		//indicador de si se cumplen todas las condiciones

		bool success  = true;

		foreach (var cond in conditions) {
			if (!DataManager.DM.CheckCondition (cond)) {
				success = false;

			}
		}

		if (success) {
			//localizamos el subobjeto PositiveReactions
			Transform positiveReactions = transform.FindChild ("PositiveReactions");


			for (int i = 0; i < positiveReactions.childCount; i++) {
				positiveReactions.GetChild (i).gameObject.GetComponent<Reaction> ().ExecuteReaction ();

			}
			//recorremos las reacciones positivas a ejecutar, y las vamos ejecutando individualmente
			/*foreach (var react in positiveReactions) {
				react.ExecuteReaction ();

			}*/
		}else{
			
			//en caso de que no se cumplan las condiciones, ejecutamos las reacciones por defecto
			Transform defaultReactions = transform.FindChild ("DefaultReactions");


			for (int i = 0; i < defaultReactions.childCount; i++) {
				defaultReactions.GetChild (i).gameObject.GetComponent<Reaction> ().ExecuteReaction ();

			}
		}
	}
}
