﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//para poder hacer referencia a los elementos UI
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

	//Referencia al objeto InventoryCanvas
	public GameObject inventoryCanvas;

	public static InventoryManager IM;

	void Awake(){
		if (IM == null) {
			IM = GetComponent<InventoryManager> ();
		}
	}
	// Use this for initialization
	void Start () {
		//actualizamos el inventario al iniciar, par4a mostrat si ya había algo en el inventario
		UpdateInventory();

	}
	/// <summary>
	/// ocaliza los datos del item en el AllItems y lo añade al inventario
	/// </summary>
	/// <param name="itenName">Iten name.</param>
	public bool AddItemInventory(string itemName){
		
		//Variable donde se almacenará el item a añadir
		Item newItem = new Item ();	

		//recorremos todos los items para buscar el item
		foreach (Item item in DataManager.DM.data.allItems) {
			//si encontramos le item recuperamos sus datos y salimos  del bucle
			if (item.name == itemName) {
				newItem.name = item.name;
				newItem.description = item.description;
				newItem.imageName = item.imageName;
				newItem.picked = item.picked;

				break;
			}
		}

		for (int i = 0; i < DataManager.DM.data.inventory.Length; i++) {
			if (DataManager.DM.data.inventory [i].name == "") {

				//introduimos el nuevo item
				DataManager.DM.data.inventory [i] = newItem;
				//actualizamos la informacion del inventario
				UpdateInventory ();
				//salimos del bucle
				return true;
			}
		}

		// En caso de no haber encontrado un hueco vacío en el inventario donde guardar el objeto, devolvemos un false.
		return false;
			
	}
	/// <summary>
	/// elimina el item del inventario
	/// </summary>
	/// <param name="itemName">Item name.</param>
	public void RemoveItemInventory(string itemName){
		foreach (Item item in DataManager.DM.data.inventory) {

			if (item.name == itemName) {
				item.name ="";
				item.imageName = "";
				item.description = "";
				item.picked = false;

				UpdateInventory ();

				break;
			}
		}
	}
	/// <summary>
	/// actualiza los objetos mostrados en le canvas de inventario
	/// </summary>
	public void UpdateInventory(){
		//recorremos el inventaario
		for (int i = 0; i < DataManager.DM.data.inventory.Length; i++) {
			 //Recuperamos la referencia a la imagen del objeto actual
			Image tempImage = inventoryCanvas.transform.GetChild (i).FindChild ("Item").GetComponent<Image> ();

			//si el hueco del inventario no está vacío
			if (DataManager.DM.data.inventory [i].name != "") {
				//recuperamos la imagen que indique
				tempImage.sprite = Resources.Load<Sprite> ("Items/" + DataManager.DM.data.inventory [i].imageName);
				Debug.Log("Items/" + DataManager.DM.data.inventory [i].imageName);
				//activamos la imagen para que sea visible
				tempImage.gameObject.SetActive (true);
			}else{
				//si el hueco esta vacio ocultamos la imagen
				tempImage.gameObject.SetActive(false);

			}


		}
	}
}
