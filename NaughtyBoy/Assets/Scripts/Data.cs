﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]

public class Data  {

	//nombre de la escena actual
	public string actualScene;

	//nombre del punto de entrada de la escena
	public string startPosition;

	//array con el estado de todas las condiciones
	public Condition[] allCondition;

	//array con el estado de todos los objetos de juego
	public Item[] allItems; 

	//array con el inventario actual del jugador
	public Item[] inventory = new Item[4];





}
