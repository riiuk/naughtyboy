﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//namespace para poder utilzar OnGroundClick

using UnityEngine.EventSystems;

//namespace para poder utilizar el nav mesh agent

using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour {

	public Animator animator;		//referencia al componente animator
	public NavMeshAgent agent; 		//referencia al componente nav mesh agent
	public float inputHoldDelay = 0.5f; //tiempo que dejaremos el input desactivado
	public float turnSpeedThreshold= 0.5f; //umbral mínimo de velocidad para permitir el giro
	public float speedDampTime = 0.1f; // suavizado del cambio de velocidad
	public float slowingSpeed = 0.175f; //velocidad de frenado
	public float turnSmoothing = 15f; //suavizado del giro
	public bool handleInput = true;	//para controlar cuando permitirá responder al click

	//clase que permite realizar una espera de un tiempo determinado
	private WaitForSeconds inputHoldWait;
	//posicion de destino
	private Vector3 destinationPosition;
	//para calculo de distancia de parada
	private const float stopDistanceProportion = 0.1f;
	//distancia a la que el navmesh puede estar añejada del cloick
	private const float navMeshSampleDistance = 4f;
	//objeto interactivo seleccionado
	private Interactable currentInteractable;



	// Use this for initialization
	void Start () {

		//evitamos que rote de forma automática
		agent.updateRotation = false;

		//preparamos el hold to wait
		inputHoldWait = new WaitForSeconds (inputHoldDelay);

		//inicialmente la posicion de destino será la posicion en la que se encuentre el objeto
		destinationPosition = transform.position;

		//Localizamos el punto definido como entrada de la escena
		GameObject startPosition = GameObject.Find (DataManager.DM.data.startPosition);
		//verificamos i ha encontrado algo

		if (startPosition) { 
			//posicionamos el jugador en ese punto
			transform.position = startPosition.transform.position;
			//rotamos al jugador en la direccion a la que apunta la posicion inicial
			transform.rotation = startPosition.transform.rotation;
		}else{
			//en caso contrario, avisamos de que no existe el punto
			Debug.LogWarning ("No se ha localizado la posición de inicio: " + DataManager.DM.data.startPosition);


		}
	}
	
	// Update is called once per frame
	void Update () {
		//soi no tiene una ruta por la que desplazarse (si no hay un path de desplazamiento pendiente, 
		if (agent.pathPending) {
			return;


		}

		//velocidad maxima del navmeshagent
		float speed = agent.desiredVelocity.magnitude;

		//cuando la distancia restante sea inferior o igual al 10% de la distancia de parada, llamamos  ala función de parada
		if (agent.remainingDistance <= agent.stoppingDistance * stopDistanceProportion) {
			
			Stopping (out speed);

		} else if (agent.remainingDistance <= agent.stoppingDistance) {
			//si se encuentra dentro del rango de stoppingDistante, deceleramos

			Slowing(out speed, agent.remainingDistance);

		}else if (speed> turnSpeedThreshold){
			//si la velocidad de giro está por encima del umbral del giro
			//rotamos el player
			Moving ();
		}

		//le indicamos al animator a que velocidad se está mpviendo el poayer
		//para que ajuste la animacion con el blend tree en consecuencia
		animator.SetFloat ("Speed", speed, speedDampTime, Time.deltaTime);


	}
	/// <summary>
	/// Realiza la parada al llegar al destino
	/// </summary>
	/// <param name="speed">Speed.</param>
	private void Stopping (out float speed){
		//paramos el desplazamiento
		agent.isStopped = true;
		//ponemos la velocidad a 0
		speed = 0;

		//si actualmente hay un objeto interactivo seleccionado al momento de hacer la parada
		//realizaremos las siguientes operaciones

		if (currentInteractable) {
			//hacemos que el jugador mire hacia el sitio adecuado
			transform.rotation = currentInteractable.interactionLocation.rotation;
			//activamos la interaccion programada en el objeto
			currentInteractable.Interact();
			//ya que hemos interactuasdo con el objeto, lo ponemos a null para futuras pulsaciones
			//con otros interactuabldes (o con el mismo)
			currentInteractable = null;

			//iniciamos una corutina de espera de tiempo

			// Comentado par apoder gestionar el control del jugador mediante ReactionLockPlayer
			//StartCoroutine (WaitForInteraction ());

		}
	}
	/// <summary>
	/// Reduce la velocidad según se acerca al destino
	/// </summary>
	/// <param name="speed">Speed.</param>
	/// <param name="distanceToDestination">Distance to destination.</param>
	private void Slowing(out float speed, float distanceToDestination){
		//paramos el desplazamiento automático y pasaremos a hacerlo nosotros
		agent.isStopped = true;

		//movemos el player hacia el destino, frenando su desplazamiento

		transform.position = Vector3.MoveTowards (transform.position, destinationPosition, slowingSpeed * Time.deltaTime);

		//cálculo de la distancia proporcional al destino
		float proportionalDistance = 1f - distanceToDestination / agent.stoppingDistance;

		//interpolamos la velocidad con la distancia restante, cuanto más cerca, más lento

		speed = Mathf.Lerp (slowingSpeed, 0f, proportionalDistance);
		//giramos de forma adecuada al jugador hacia el objeto interactivo
		Quaternion targetRotation = currentInteractable ? currentInteractable.interactionLocation.rotation: transform.rotation;
		/*esa linea es lo mismo que= 
		 							if(currentInteractable != null){
										targetRotation = currentInteractable.interactionLocation.rotation;
									}else{
										targetRotation = transform.rotation; */
		//ya que tenemos definido el targetrotation, interpolamos el giro para suavizarlo en funcion de la distancia proporcional al target
		transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, proportionalDistance);

	}
	/// <summary>
	/// Rotamos al personaje
	/// </summary>
	private void Moving(){
		//averiguamos el ángulo al que tiene que rotar para encarar al destino
		Quaternion targetRotation = Quaternion.LookRotation (agent.desiredVelocity);

		transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, turnSmoothing * Time.deltaTime);

	}

	public void OnGroundClick (BaseEventData data){
		//si no hay control del input, no hacemos nada, salimos del metodo de forma prematura, sin hacer nada

		if (!handleInput) {
			return;
		}

		//si detectamos la pulsacion sobre el ground, desactivamos el posible objeto
		//interactivo que puede haber sido pulsado previamente

		currentInteractable = null;

		//hacemos un cast del objeto recuperado por el evento click
		//para volcarlo a un objeto que nos permita gestionarlo de forma adecuada

		PointerEventData pData = (PointerEventData)data;

		//variable para almacenar el impacto que se ha producido el click
		NavMeshHit hit;

		//mediante el pointer event data, podemos localizar un impacto en el navmesh
		if (NavMesh.SamplePosition (pData.pointerCurrentRaycast.worldPosition, out hit, navMeshSampleDistance, NavMesh.AllAreas)) {

			destinationPosition = hit.position;

		} else {
			//si no localicamos un punto de impacto en el navmesh, movemos el jugador en la direccion del click 
			destinationPosition = pData.pointerCurrentRaycast.worldPosition;

		}
		//fijamos en el navmesh agent el destino del desplazamiento
		agent.SetDestination (destinationPosition);
		//iniciamos el desplazamiento del jugador
		agent.isStopped = false;
		}


	public void OnInteractableClick (Interactable interactable){
		//si no hay control del input, no hacemos nada más
		if (!handleInput) {
			return;
		}

		//fijamos el objeto interactivo actual
		currentInteractable = interactable;
		//el objetivo del desplazaminto, será interacion location del objeto interactivo
		destinationPosition = currentInteractable.interactionLocation.position;

		//fijamos el destino en el navmesh agent
		agent.SetDestination (destinationPosition);

		agent.isStopped = false;



	}
	private IEnumerator WaitForInteraction(){
		//al inicio dejo de gestionar el input
		handleInput = false;

		//realizamos una espera de 0.5 segundos
		yield return inputHoldWait;
	
		while (!animator.GetCurrentAnimatorStateInfo (0).IsTag ("Locomotion")) {
			yield return null; 

		}

		handleInput = true;
	}

}

